import React, { useState, useEffect } from "react";
import Home from "./components/Home";
import { Route, Routes } from "react-router-dom";
import NotFound from "./components/NotFound";
import Header from "./components/Header/Header";
import Cart from "./components/Cart";
import Favorite from "./components/Favorite";



function App() {
  // localStorage.clear()

  return (
    <div className="container">
    <Header/>
    <Routes>
      <Route path="/" element={<Home/>}/>
      <Route path="/cart" element={<Cart/>}/>
      <Route path="/star" element={<Favorite/>}/>
      <Route path="*" element={<NotFound/>}/>
    </Routes>
    </div>
  );
}

export default App;
