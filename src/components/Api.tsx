export const fetchData = async()=>{
    try{
        const response = await fetch ('/public/products.json');
        if(response.ok){
            const data = await response.json();
            return data;
        }else{
            throw new Error('Error receiving data');
        }
    }catch (err){
        throw(err);
    }
};