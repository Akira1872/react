import React from 'react'
import Title from './Title'
import BannerBody from './BannerBody'
import styled from 'styled-components'

export const BannerContainer = styled.section`
background-image: url('/public/4.jpg');
background-size: cover;
  background-position: center;
  background-repeat: no-repeat;
  height: 50vh;
  width: 100%;
  display: flex;
  align-items: flex-end;
  justify-content: space-between;

`

function Banner() {
  return (
    <BannerContainer>
        <Title></Title>
        <BannerBody></BannerBody>
    </BannerContainer>
  )
}

export default Banner

