import React from 'react'
import styled from 'styled-components';

export const PHeader = styled.p`
color: #094535;   
  font-family: Montserrat;
  font-size: 16px;
  font-style: normal;
  font-weight: 400;
  line-height: normal;
  text-align: right;
  margin-bottom: 20vh;
    width: 30vw;
    margin-right: 10vw;
`

interface Props{
    children?: string;
}
function BannerBody({children}:Props) {
  return (
    <PHeader>{children}</PHeader>
  )
}

export default BannerBody