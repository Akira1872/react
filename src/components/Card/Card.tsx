import React, { useState, useEffect } from 'react';
import Cart from './Cart';
import DeleteSvg from './DeleteSvg';
import Star from './Star';
import Img from './Img';
import Price from "./Price";
import Name from './Name';
import Color from "./Color";
import { CardS, CardPrice, BoxPrice} from "./CardStyles";
import { DataItem } from '../../state/FavoritesCounter/cardsSlice';

interface CardProps {
  data: DataItem;
  cartClick: ()=>void;
  hideCart?: boolean;
  hideDelete?: boolean;
  deleteClick: ()=>void;
}

function Card({ data, cartClick, hideCart = false, hideDelete = false, deleteClick}: CardProps) {
  return (
    <CardS className={`card${data.artikel}`}>
      <Img src={data.src} alt={data.name} />
      <CardPrice>
      {!hideCart && <Cart artikel={data.artikel} cartClick={cartClick}/>}
      {!hideDelete && <DeleteSvg id={data.artikel}/>}
        <Star id={data.artikel} cardInfo={data}/>
        <BoxPrice>
          <Price>{data.price}</Price>
          <Color>{data.color}</Color>
          <Name>{data.name}</Name>
        </BoxPrice>
      </CardPrice>
    </CardS>
  );
}

export default Card;