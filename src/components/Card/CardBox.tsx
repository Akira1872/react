import React, { useState, useEffect } from "react";
import { fetchData } from "../Api";
import * as styles from "./CardStyles";
import FirstModal from "../Modal/FirstModal";
import SecondModal from "../Modal/SecondModal";
import Filtr from "./Filtr";
import GroupedCards from "./GroupedCards";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../../state/store";
import { fetchCardData } from "../../state/FavoritesCounter/modalActions";
import { DataItem } from "../../state/FavoritesCounter/cardsSlice";
import { toggleModal} from "../../state/FavoritesCounter/modalSleice";


function Cards() {

  const dispatch = useDispatch();

  const data = useSelector((state: RootState) => state.cards.data);
  const modals = useSelector((state: RootState) => state.modals);
  const cartItems = useSelector((state: RootState) => state.cart.items);
  const selectedArtikel = useSelector((state: RootState) => state.modals.selectedArtikel);

  

  useEffect(() => {
    dispatch(fetchCardData() as any);
  }, [dispatch]);

  const handleSvgClick = (artikel: string) => {
    const selectedItems = JSON.parse(localStorage.getItem('selectedModalItems') || '[]');
    const isSelected = selectedItems.includes(artikel);

    
  const isItemInCart = cartItems.some(item => item.artikel === artikel);
  
    console.log(isItemInCart);
    
    if (isItemInCart) {
      dispatch(toggleModal({ type: 'first', artikel }));
    } else if (!isSelected) {
      dispatch(toggleModal({ type: 'second', artikel }));
    }
  };

  const groupByColor = (data: DataItem[]) => {
    const groupedData: { [key: string]: DataItem[] } = {};
    data.forEach((item) => {
      const color = item.color;
      if (!groupedData[color]) {
        groupedData[color] = [];
      }
      groupedData[color].push(item);
    });
    return groupedData;
  };

  const groupedData = groupByColor(data);

  const listItems = [
    "Bouquets",
    "Compositions ",
    "Wedding bouquets ",
    "Indoor flowers ",
  ];

// localStorage.clear();

  return (
    <styles.CardContainer>
      <Filtr onClick={() => console.log("click")}>{listItems}</Filtr>
      <GroupedCards groupedData={groupedData} cartClick={handleSvgClick}/>
      {modals.first && <FirstModal id={selectedArtikel}/>}
      {modals.second && <SecondModal/>}
    </styles.CardContainer>
  );
}

export default Cards;