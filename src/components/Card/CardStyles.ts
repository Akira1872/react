import styled from "styled-components";

export const CardListBox = styled.div`
display: flex;
justify-content: space-around;
width: 100%;
margin: 3vh;
background-image: url('/public/5.jpg');
background-attachment: fixed;
background-size: cover;
background-position: center;
background-repeat: no-repeat;`

export const CardS = styled.div`
display: flex;
flex-direction: column;
justify-content: center;
border-radius: 16px;
background: #FFF;
box-shadow: 0px 4px 4px 0px rgba(0, 0, 0, 0.25);
margin-top: 1vw;
width: 200px;
height: 324px;
`;

export const CardPrice = styled.div`
`;

export const CardContainer = styled.div`
width: 100%;
display: flex;
flex-direction: column;
align-items: center;
`

export const GroupBox = styled.div`
width: 100%;
display: flex;
flex-direction: column;
align-items: center;   
`

export const GroupTitle = styled.div`
margin-left: 72vw;
color: #EEAE5E;
text-align: center;
font-family: 'Playfair Display', serif;
font-size: 20px;
font-style: normal;
font-weight: 400;`

export const BoxPrice = styled.div`
display: flex;
flex-direction: column;
align-items: flex-end;
margin: 10px;
`;