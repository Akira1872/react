import React from 'react'
import styled from 'styled-components';

export const CardN = styled.div`
  color: #16342C;
  font-family: 'Quicksand', sans-serif;
  font-size: 16px;
  font-style: normal;
  font-weight: 400;
  line-height: normal;
`;

interface Props {
  children: string;
}

function Name({ children }: Props) {
  return (
    <CardN>{children}</CardN>
  )
}

export default Name
