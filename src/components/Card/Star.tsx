import React, { useState, useEffect } from "react";
import styled from "styled-components";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../../state/store";
import {updateStarColor, removeStarColor} from "../../state/FavoritesCounter/counterSlice";
import { selectCard, unselectCard } from "../../state/FavoritesCounter/cardSlice";
import { DataItem } from "../../state/FavoritesCounter/cardSlice";

export const SvgStar = styled.svg`
  width: 25px;
  height: 40px;
  fill: ${(props) => props.fill || "white"};
  cursor: pointer;
  margin-left: 1vw;
`;

interface Props {
  id?: string;
  cardInfo?: DataItem;
}

function Star({ id, cardInfo }: Props) {
  const dispatch = useDispatch();
  const selectedCardId = useSelector((state: RootState) => state.card.selectedCardId);
  const actualId = id || "";
  const fillColor = useSelector(
    (state: RootState) => state.star.starColors[actualId] || "white"
  );

  const handleSvgClick = () => {
    if (!cardInfo) return; 

    const isSelected = !!fillColor && fillColor !== "white";

    if (isSelected) {
      dispatch(removeStarColor(cardInfo.artikel)); 
      dispatch(unselectCard(cardInfo));
    } else {
      dispatch(updateStarColor({ id: cardInfo.artikel, color: "#16342C" })); 
      dispatch(selectCard(cardInfo)); 
    }
  };

  return (
    <SvgStar
      onClick={() => {
        handleSvgClick();
      }}
      fill={fillColor}
      viewBox="0 0 24 24"
      xmlns="http://www.w3.org/2000/svg"
      stroke="#16342C"
    >
      <g id="SVGRepo_bgCarrier" strokeWidth="0" />
      <g
        id="SVGRepo_tracerCarrier"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <g id="SVGRepo_iconCarrier">
        {" "}
        <path
          fillRule="evenodd"
          clipRule="evenodd"
          d="M12 6.00019C10.2006 3.90317 7.19377 3.2551 4.93923 5.17534C2.68468 7.09558 2.36727 10.3061 4.13778 12.5772C5.60984 14.4654 10.0648 18.4479 11.5249 19.7369C11.6882 19.8811 11.7699 19.9532 11.8652 19.9815C11.9483 20.0062 12.0393 20.0062 12.1225 19.9815C12.2178 19.9532 12.2994 19.8811 12.4628 19.7369C13.9229 18.4479 18.3778 14.4654 19.8499 12.5772C21.6204 10.3061 21.3417 7.07538 19.0484 5.17534C16.7551 3.2753 13.7994 3.90317 12 6.00019Z"
          stroke="#16342C"
          strokeWidth="2"
          strokeLinecap="round"
          strokeLinejoin="round"
        />{" "}
      </g>
    </SvgStar>
  );
}

export default Star;
