import React, { useState, useEffect } from "react";
import { fetchData } from "./Api";
import Card from "./Card/Card";
import FooterContainer from "./Footer/FooterContainer";
import FirstModal from "./Modal/FirstModal";
import { useSelector} from "react-redux";
import { RootState } from "../state/store";


function Cart() {

  const cart = useSelector((state: RootState) => state.cart.items);
  console.log(cart);
  
  return (
    <div className="container">
      <div className="con">       
      {cart.map((i) => {
          return(
            <Card
              key={i.artikel}
              data={i}
              cartClick={() => console.log('Add to cart clicked')} 
              hideCart={true}
              hideDelete={false}
              deleteClick={() => console.log('Delete clicked')}
            />
          )
        })}
      </div>
      <FooterContainer/>
    </div>
  );
}


export default Cart
