import React from 'react'
import EventImg from './EventImg'
import styled from 'styled-components'
import EventTitle from './EventTitle';

export const Container = styled.section`
width: 95vw;
display: flex;
flex-direction: column;
 height: 40vh;
justify-content: space-between;
margin: 3vw;
border: solid 1px;
`;
export const EventBox = styled.section`

display: flex;
justify-content: space-around;
margin-bottom: 3vh;

`;

function EventSection() {
  return (
    <Container>
    <EventTitle onClick={()=> console.log('click')}>Events</EventTitle>
    <EventBox>
    <EventImg src = "/public/5.jpg" alt='event1'/>
    <EventImg src = "/public/10.jpg" alt='event1'/>
    <EventImg src = "/public/8.jpg" alt='event1'/>
    </EventBox>
    </Container>
  )
}

export default EventSection