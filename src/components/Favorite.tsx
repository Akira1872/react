
import Card from "./Card/Card";
import FooterContainer from "./Footer/FooterContainer";
import { useSelector} from "react-redux";
import { RootState } from "../state/store";

function Favorite() {  
  const favorite = useSelector((state: RootState) => state.card.itemsFav);
  
  return (
    <div className="container">
      <div className="con">       
      {favorite.map((i) => {
          return(
            <Card
              key={i.artikel}
              data={i}
              cartClick={() => console.log('Add to cart clicked')} 
              hideCart={true}
              hideDelete={true}
              deleteClick={() => console.log('Delete clicked')}
            />
          )
        })}
      </div>
      <FooterContainer/>
    </div>
  );
}


export default Favorite;