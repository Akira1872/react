import React from 'react'
import styled from 'styled-components'
import FooterBody from './FooterBody';

export const Footer = styled.div`
height: 25vh;
background-color: #16342C;
display: flex;
align-items: center;
width: 100%;`

function FooterContainer() {
  return (
    <Footer>
      <FooterBody>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Hic culpa error maiores facere natus aperiam amet? Qui expedita, excepturi in repellendus ea architecto corrupti quidem odit, facilis voluptate ad id!</FooterBody>
    </Footer>
  )
}

export default FooterContainer