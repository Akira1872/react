import React, {useEffect} from "react";
import HeaderList from "./Headerlist";
import Inst from "./Insta";
import Threads from "./Threads";
import Cart from "../Card/Cart";
import Star from "../Card/Star";
import styled from "styled-components";
import { Link } from 'react-router-dom';
import { useSelector } from "react-redux";
import { RootState } from "../../state/store";



export const Head = styled.header`
  display: flex;
  justify-content: space-evenly;
  align-items: center;
  width: 85vw;
  margin: 20px 0 20px;
`;

const listItems = ["Home", "Shop", "Event", "Service", "Contact"];
const toPaths = ["/", "/shop", "/event", "/service", "/contact"];

function Header() {
  const selectedCartCount = useSelector((state: RootState) => state.cart.selectedItemCount);
  const selectedStarCount = useSelector((state: RootState) => state.star.selectedStarCount);

  return (
    <Head>
      <Link to="/cart">
      <Cart />
      </Link>
      <div>{selectedCartCount}</div>
      <Link to="/star">
      <Star/>
      </Link>
      <div>{selectedStarCount}</div>
      <HeaderList toPaths={toPaths}>{listItems}</HeaderList>
      <Inst onClick={() => console.log("click")} />
      <Threads onClick={() => console.log("click")} />
    </Head>
  );
}

export default Header;
