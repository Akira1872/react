import React, { useState, useEffect } from "react";
import Banner from "./Banner/Banner";
import EventSection from "./Events/EventSection";
import Cards from "./Card/CardBox";
import FooterContainer from "./Footer/FooterContainer";

function Home() {
  return (
    <div className="container">
        <Banner/>
        <EventSection/>
        <Cards/> 
        <FooterContainer/>
    </div>
  );
}


export default Home;