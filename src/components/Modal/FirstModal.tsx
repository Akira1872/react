import React, { useState } from "react";
import ModalWrapper from "./ModalWrapper";
import ModalHeader from "./ModalHeader";
import ModalBody from "./ModalBody";
import ModalFooter from "./ModalFooter";
import ModalClose from "./ModalClose";
import Modal from "./Modal";
import Img from "./Img";
import "./Style.scss";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../../state/store";
import { closeModal } from "../../state/FavoritesCounter/modalSleice";
import { removeFromCart } from "../../state/FavoritesCounter/cartSlice";

interface Props{
  id: string | null | undefined;
  closeModalHandler?: ()=>void;
}

function FirstModal({id, closeModalHandler}:Props) {
  const dispatch = useDispatch();
  const data = useSelector((state: RootState) => state.cards.data);
  const cartItems = useSelector((state: RootState) => state.cart.items);
  
  const selectedCard =  data.find((item) => item.artikel === id);
  
  const removeCart = () => {
    const isItemInCart = cartItems.some(item => item.artikel === id);
  
    if (isItemInCart) {
      dispatch(removeFromCart({ artikel: id }));
    }
  };

  

  const btnFirstClick = ()=>{
    dispatch(closeModal());
 }

  const btnSecondClick = ()=>{
    if (id) {
      removeCart();
    }
    dispatch(closeModal());
  };

 if (!selectedCard) {
  return null;
}

  return (
    <Modal>
      <ModalWrapper
        type="first"
        onClick={(e) => {
          e.stopPropagation();
        }}
      >
        <ModalClose onClick={btnFirstClick} />
        <Img src={selectedCard.src}/>
        <ModalHeader>Remove from the shopping cart ?</ModalHeader>
        <ModalBody>
          By clicking the “Yes, Delete” button, {selectedCard.name} will be deleted.
        </ModalBody>
        <ModalFooter
          firstText="NO, CANCEL"
          firstClick={btnFirstClick}
          secondText="YES, DELETE"
          secondaryClick={btnSecondClick}
        />
      </ModalWrapper>
    </Modal>
  );
}

export default FirstModal;
