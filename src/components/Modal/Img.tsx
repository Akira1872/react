import React from 'react'

import styled from "styled-components";

export const ModalI = styled.img`
width: 250px;
height: 250px;
margin-left: 156px;
border-radius: 140px;

`

interface Props{
   src?: string;
}
function Img({src}:Props) {
  return (
    <ModalI src={src} alt="img" />
  )
}

export default Img