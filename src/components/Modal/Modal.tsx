import React, { ReactNode, useCallback } from 'react';
import styled from "styled-components";
import { useDispatch } from 'react-redux';
import { closeModal } from '../../state/FavoritesCounter/modalSleice';

export const Mod = styled.div`
position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background: rgba(0, 0, 0, 0.5); /* Півпрозорий чорний колір */
  display: flex;
  justify-content: center;
  align-items: center;`

interface Props{
    children: ReactNode;
}

function Modal({children}:Props) {
  const dispatch = useDispatch();

  const handleOutsideClick = useCallback(
    (e: React.MouseEvent<HTMLDivElement, MouseEvent>) => {
      if (e.target === e.currentTarget) {
        dispatch(closeModal());
      }
    },
    [dispatch]
  );
  return (
    <Mod onClick={handleOutsideClick} className='modal-overlay' >{children}</Mod>
  )
}

export default Modal