import React from 'react';
import styled from "styled-components";

export const ModalCl = styled.span`
display: flex;
justify-content: flex-end;
margin: 20px;
cursor: pointer`;

interface Props{
    onClick: ()=>void;
}

function ModalClose({onClick}:Props) {
  return (
    <ModalCl onClick={onClick}>✖</ModalCl>
  )
}

export default ModalClose