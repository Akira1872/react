import React from 'react';
import styled from "styled-components";

export const ModalF = styled.div`
display: flex;
justify-content: space-around;
margin-bottom: 56px`;

interface Props{
    firstText: string;
    secondText?: string;
    firstClick: ()=>void;
    secondaryClick?: ()=>void;
}

function ModalFooter({firstClick, secondaryClick, firstText, secondText}:Props) {
  return (
    <ModalF>
        {firstText && (
            <button className='btn_footer1' onClick={firstClick}>{firstText}</button>
        )}
        {secondText && (
            <button className='btn_footer2' onClick={secondaryClick}>{secondText}</button>
        )}
    </ModalF>
  )
}

export default ModalFooter