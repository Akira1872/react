import React, { ReactNode } from 'react';
import styled from "styled-components";

export const FirstModalW = styled.div` 
width: 560px;
height: 558px;
border-radius: 16px;
background: #FFF;
box-shadow: 0px 4px 4px 0px rgba(0, 0, 0, 0.25);
display: flex;
flex-direction: column;
justify-content: space-between;
`;

export const SecondModalW = styled.div`
width: 560px;
height: 333px;
border-radius: 16px;
background: #FFF;
box-shadow: 0px 4px 4px 0px rgba(0, 0, 0, 0.25);
display: flex;
flex-direction: column;
justify-content: space-between;
` 


interface Props{
    children: ReactNode;
    onClick: (e: React.MouseEvent) => void;
    type: 'first' | 'second';
}

function ModalWrapper({children, onClick, type}: Props) {
  return (
    <>
      {type === 'first' && (
        <FirstModalW onClick={onClick}>
          {children}
        </FirstModalW>
      )}
      {type === 'second' && (
        <SecondModalW onClick={onClick}>
          {children}
        </SecondModalW>
      )}
    </>
  )
}

export default ModalWrapper