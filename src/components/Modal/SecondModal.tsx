import React, { useState, useCallback } from "react";
import ModalWrapper from "./ModalWrapper";
import ModalHeader from "./ModalHeader";
import ModalBody from "./ModalBody";
import ModalFooter from "./ModalFooter";
import ModalClose from "./ModalClose";
import Modal from "./Modal";
import "./Style.scss";
import { closeModal } from "../../state/FavoritesCounter/modalSleice";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../../state/store";
import { addToCart } from "../../state/FavoritesCounter/cartSlice";


function SecondModal() {
  const dispatch = useDispatch();
  const selectedArtikel = useSelector((state: RootState) => state.modals.selectedArtikel);
  const data = useSelector((state: RootState) => state.cards.data);

  const selectedCard = data.find((item) => item.artikel === selectedArtikel);
  
  const btnClick = () =>{
    if (selectedCard) {
      dispatch(closeModal());
      dispatch(addToCart(selectedCard));
      console.log('Data added to cart:', selectedCard);
    }
  };

  const closeModalHandler = () => {
    dispatch(closeModal()); 
  }
  
  if (!selectedCard) {
    return null;
  }

  return (
    <Modal>
      <ModalWrapper
        type="second"
        onClick={(e) => {
          e.stopPropagation();
        }}
      >
        <ModalClose onClick={closeModalHandler} />
        <ModalHeader>{selectedCard.name}</ModalHeader>
        <ModalBody>{selectedCard.color}</ModalBody>
        <ModalFooter
          firstText="ADD TO CART"
          firstClick={btnClick}
        />
      </ModalWrapper>
    </Modal>
  );
}

export default SecondModal;



