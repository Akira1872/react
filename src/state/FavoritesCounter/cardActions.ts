import { Middleware } from '@reduxjs/toolkit';
import { RootState } from '../store';
import { updateStarColor } from './counterSlice';

const localStorageMiddleware: Middleware<{}, RootState> = (store) => (next) => (action) => {
  const result = next(action);

  const { star } = store.getState();
  if (Object.keys(star.starColors).length === 0) {
    const persistedSelectedStarColors = localStorage.getItem('selectedStarColors');
    if (persistedSelectedStarColors) {
      store.dispatch(updateStarColor(JSON.parse(persistedSelectedStarColors)));
    }
  }

  return result;
};

export default localStorageMiddleware;

