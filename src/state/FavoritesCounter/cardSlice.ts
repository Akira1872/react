import { createSlice, PayloadAction } from '@reduxjs/toolkit';

interface SelectedCardState {
  selectedCardId: string | null; 
  itemsFav: DataItem[],

}

export interface DataItem {
  name: string;
  price: string;
  src: string;
  artikel: string;
  color: string;
}

const persistedSelectedCardId = localStorage.getItem('selectedCardId');
const persistedCartItems = localStorage.getItem('cardFavorites');


const initialState: SelectedCardState = {
  itemsFav: persistedCartItems ? JSON.parse(persistedCartItems) : [],
  selectedCardId: persistedSelectedCardId ? persistedSelectedCardId : null, 
};

const selectedCardSlice = createSlice({
  name: 'card',
  initialState,
  reducers: {
    selectCard(state, action: PayloadAction<DataItem>) {
      state.itemsFav.push(action.payload); 
      state.selectedCardId = action.payload.artikel; 
      localStorage.setItem('selectedCardId', action.payload.artikel);
      localStorage.setItem('cardFavorites', JSON.stringify(state.itemsFav));
    },
    unselectCard(state, action: PayloadAction<DataItem>) {
      const index = state.itemsFav.findIndex(item => item.artikel === action.payload.artikel);
      if (index !== -1){
        state.itemsFav.splice(index, 1);
        state.selectedCardId = null; 
        localStorage.removeItem('selectedCardId');
        localStorage.setItem('cardFavorites', JSON.stringify(state.itemsFav));
      }
      
    },
  },
});

export const { selectCard, unselectCard } = selectedCardSlice.actions;
export default selectedCardSlice.reducer;