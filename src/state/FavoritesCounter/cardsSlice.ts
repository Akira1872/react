import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { fetchCardData } from './modalActions';

interface CardDataState {
  data: DataItem[];
}

export interface DataItem {
  name: string;
  price: string;
  src: string;
  artikel: string;
  color: string;
}

const initialState: CardDataState = {
  data: [],
};

const cardsSlice = createSlice({
  name: 'cards',
  initialState,
  reducers: {
    setData(state, action: PayloadAction<DataItem[]>) {
      state.data = action.payload;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(fetchCardData.fulfilled, (state, action) => {
      state.data = action.payload;
    });
  },
});

export const { setData } = cardsSlice.actions;
export default cardsSlice.reducer;

