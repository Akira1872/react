import { createSlice, PayloadAction } from '@reduxjs/toolkit';

interface CartState{
    items: DataItem[],
    selectedItemCount: number,
    selectedColors: Record<string, string>;
}

export interface DataItem {
    name: string;
    price: string;
    src: string;
    artikel: string;
    color: string;
    isAddedToCart: boolean;
  }

const persistedCartItems = localStorage.getItem('cartItems');
const persistedCartCount = localStorage.getItem('cartCount');
const persistedSelectedColors = localStorage.getItem('selectedColors');

const initialState: CartState = {
  items: persistedCartItems ? JSON.parse(persistedCartItems) : [],
  selectedItemCount: persistedCartCount ? parseInt(persistedCartCount, 10) : 0,
  selectedColors: persistedSelectedColors ? JSON.parse(persistedSelectedColors) : {},
};


const cartSlice = createSlice({
  name: 'cart',
  initialState,
  reducers: {
    addToCart: (state, action) => {
      const dataItem = action.payload as DataItem;
      state.items.push({ ...dataItem, isAddedToCart: true });
      state.selectedItemCount = state.items.length;
      localStorage.setItem('cartItems', JSON.stringify(state.items));
      localStorage.setItem('cartCount', state.selectedItemCount.toString());
      state.selectedColors[dataItem.artikel] = dataItem.color;
      localStorage.setItem('selectedColors', JSON.stringify(state.selectedColors));
    },
    removeFromCart: (state, action) => {
      const dataItem = action.payload as DataItem;
      const index = state.items.findIndex(item => item.artikel === dataItem.artikel);
      if (index !== -1) {
        state.items.splice(index, 1);
        state.selectedItemCount = state.items.length;
        localStorage.setItem('cartItems', JSON.stringify(state.items));
        localStorage.setItem('cartCount', state.selectedItemCount.toString());
        delete state.selectedColors[dataItem.artikel];
        localStorage.setItem('selectedColors', JSON.stringify(state.selectedColors));
      }
    },
  },
});

export const { addToCart, removeFromCart } = cartSlice.actions;
export default cartSlice.reducer;