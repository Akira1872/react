import {createSlice,  PayloadAction} from "@reduxjs/toolkit";

interface StarState{
    starColors: Record<string, string>;
    selectedStarCount: number;
}

const persistedSelectedStarCount = localStorage.getItem('selectedStarCount');
const persistedSelectedStarColors = localStorage.getItem('selectedStarColors');

const initialState: StarState = {
    starColors: persistedSelectedStarColors ? JSON.parse(persistedSelectedStarColors) : {},
    selectedStarCount: persistedSelectedStarCount ? parseInt(persistedSelectedStarCount, 10) : 0,
  };

  const starSlice = createSlice({
    name: 'star',
    initialState,
    reducers: {
      updateStarColor(state, action: PayloadAction<{ id: string; color: string }>) {
        const { id, color } = action.payload;
        state.starColors[id] = color; 
        state.selectedStarCount = Object.keys(state.starColors).length;
        localStorage.setItem('selectedStarCount', state.selectedStarCount.toString());
        localStorage.setItem('selectedStarColors', JSON.stringify(state.starColors));
      },
      removeStarColor(state, action: PayloadAction<string>) {
        const idToRemove = action.payload;
        delete state.starColors[idToRemove]; 
        state.selectedStarCount = Object.keys(state.starColors).length;
        localStorage.setItem('selectedStarCount', state.selectedStarCount.toString());
        localStorage.setItem('selectedStarColors', JSON.stringify(state.starColors));
      },
    },
  });
  
  export const { updateStarColor, removeStarColor } = starSlice.actions;
  export default starSlice.reducer;