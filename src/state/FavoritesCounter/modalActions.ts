import { createAction, createAsyncThunk } from '@reduxjs/toolkit';
import { fetchData } from '../../components/Api';

export const handleSvgClick = createAction<string>('cards/handleSvgClick');

export const fetchCardData = createAsyncThunk(
  'cards/fetchCardData',
  async () => {
    try {
      const data = await fetchData(); 
      return data;
    } catch (error) {
      throw Error('Error fetching data');
    }
  }
);