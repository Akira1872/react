import { createSlice, PayloadAction } from '@reduxjs/toolkit';

interface ModalsState {
  first: boolean;
  second: boolean;
  selectedArtikel: string | null;
}

const initialState: ModalsState = {
  first: false,
  second: false,
  selectedArtikel: null,
};

const modalsSlice = createSlice({
  name: 'modals',
  initialState,
  reducers: {
    toggleModal(state, action: PayloadAction<{ type: string; artikel: string }>) {
      const { type, artikel } = action.payload;
      if (type === 'first') {
        state.first = !state.first;
      } else if (type === 'second') {
        state.second = !state.second;
      }
      state.selectedArtikel = artikel;
    },
    setModalSelectedArtikel(state, action: PayloadAction<string>) {
        state.selectedArtikel = action.payload;
      },
    closeModal(state) {
      state.first = false;
      state.second = false;
    },
  },
});

export const { toggleModal, closeModal} = modalsSlice.actions;
export default modalsSlice.reducer;