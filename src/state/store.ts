import { configureStore, applyMiddleware} from "@reduxjs/toolkit";
import thunk from 'redux-thunk';
import starReducer from "./FavoritesCounter/counterSlice";
import cardReducer from "./FavoritesCounter/cardSlice";
import cardsReducer from "./FavoritesCounter/cardsSlice";
import modalsReducer from "./FavoritesCounter/modalSleice";
import cartReducer from "./FavoritesCounter/cartSlice"


export const store = configureStore({
    reducer: {
        star: starReducer,
        card: cardReducer,
        cards: cardsReducer,
        modals: modalsReducer,
        cart: cartReducer,
    },
    middleware: [thunk],
})

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;